package com.example.JavaRiga13Ex.ex11;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/pc-game")
public class PCGameController {

    @GetMapping
    public String getPcGameForm(final ModelMap modelMap) {
        modelMap.addAttribute("createMessage", "Create PC Game");
        modelMap.addAttribute("pcGameForm", new PCGameForm());
        return "pcgame";
    }
}
