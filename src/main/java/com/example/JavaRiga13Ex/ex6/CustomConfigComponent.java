package com.example.JavaRiga13Ex.ex6;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;

@Component
@Validated
@ConfigurationProperties(prefix = "pl.sdacademy.zad6")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomConfigComponent {

    @Email(message = "wrong email")
    private String email;
    private String firstName;
    @Length(min = 3, max = 20)
    private String lastName;
    private String address;
    @Min(18)
    private int age;
    @NotEmpty
    private List<String> values;
    @NotEmpty
    private Map<String,String> customAttributes;

    @AssertTrue
    private boolean isAddressValid(){
        return address.split(" ").length == 2;
    }

}
