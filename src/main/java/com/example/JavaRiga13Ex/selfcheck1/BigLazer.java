package com.example.JavaRiga13Ex.selfcheck1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Primary
public class BigLazer implements Gun {
    @Override
    public int shoot() {
        log.info("WAB ! WAB WAB! WAB! ");
        return 10;
    }
}
