package com.example.JavaRiga13Ex.ex1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component("dummyLoggerEx1")
@Slf4j
public class DummyLogger implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        log.info("Hello from task1");
    }
}
