package com.example.JavaRiga13Ex.ex10;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookTest implements CommandLineRunner {

    private final BookRepo bookRepo;

    @Override
    public void run(String... args) throws Exception {
        bookRepo.saveAll(List.of(
                Book.builder().author("Me").ISBN("a").pagesNum(10).title("Jit").build(),
                Book.builder().author("You").ISBN("b").pagesNum(101).title("Kit").build(),
                Book.builder().author("He").ISBN("c").pagesNum(104).title("Bit").build(),
                Book.builder().author("She").ISBN("d").pagesNum(310).title("Whit").build(),
                Book.builder().author("She").ISBN("d").pagesNum(3100).title("Whit2").build(),
                Book.builder().author("She").ISBN("d").pagesNum(31000).title("Whit3").build()
        ));

        log.info(bookRepo.findAllByPagesNumIsBetween(101,200).toString());
        log.info(bookRepo.findAllByTitleStartsWith("Ki").toString());
        log.info(bookRepo.findTop3ByAuthorOrderByPagesNumDesc("She").toString());
        log.info(bookRepo.findAllByTitle("Jit").toString());
        log.info(bookRepo.findFirstByISBN("d").toString());
        log.info(bookRepo.findWherePagesNumIsGreaterThanX(200).toString());
    }
}
