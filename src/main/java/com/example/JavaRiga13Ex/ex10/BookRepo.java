package com.example.JavaRiga13Ex.ex10;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepo extends JpaRepository<Book, Long> {

    List<Book> findAllByTitle(String title);

    Book findFirstByISBN(String isbn);

    Book findByAuthorAndISBN(String author, String isbn);

    List<Book> findTop3ByAuthorOrderByPagesNumDesc(String author);

    List<Book> findAllByTitleStartsWith(String start);

    List<Book> findAllByPagesNumIsBetween(int min, int max);

    @Query("SELECT b FROM Book as b where b.pagesNum > :x")
    List<Book> findWherePagesNumIsGreaterThanX(@Param("x") int x);
}
