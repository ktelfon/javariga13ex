package com.example.JavaRiga13Ex.ex9;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class FileDataWrapper {
    private List<FileData> fileData;
}
