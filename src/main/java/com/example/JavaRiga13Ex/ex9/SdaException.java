package com.example.JavaRiga13Ex.ex9;

public class SdaException extends RuntimeException {
    public SdaException(final String message) {
        super(message);
    }
}
