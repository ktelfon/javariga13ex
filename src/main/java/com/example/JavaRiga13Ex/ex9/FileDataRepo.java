package com.example.JavaRiga13Ex.ex9;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.UUID;

public interface FileDataRepo extends JpaRepository<FileData, UUID> {
}
