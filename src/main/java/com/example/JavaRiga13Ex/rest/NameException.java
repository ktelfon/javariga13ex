package com.example.JavaRiga13Ex.rest;

public class NameException extends Throwable {
    public NameException(String message) {
        super(message);
    }
}
