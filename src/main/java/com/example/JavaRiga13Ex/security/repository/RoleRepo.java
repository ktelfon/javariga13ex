package com.example.JavaRiga13Ex.security.repository;

import com.example.JavaRiga13Ex.security.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
