package com.example.JavaRiga13Ex.security.repository;

import com.example.JavaRiga13Ex.security.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, String> {
}
