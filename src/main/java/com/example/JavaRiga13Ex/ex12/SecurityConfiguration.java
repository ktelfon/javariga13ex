package com.example.JavaRiga13Ex.ex12;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Profile("ex12")
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/api/cars").hasAnyRole("ADMIN", "CARS")
                .antMatchers(HttpMethod.POST, "/api/cars").authenticated()
                .antMatchers("/api/users/**").hasAuthority("ROLE_USER_ADMIN")
                .anyRequest().permitAll()
                .and()
                .httpBasic()
                .and()
                .logout()
                .and()
                .headers().frameOptions().disable();

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password("{noop}123")
                .roles("ADMIN", "CARS")
                .and()
                .withUser("admin2")
                .password("{noop}123")
                .authorities("ROLE_USER_ADMIN")
                .and()
                .withUser("admin3")
                .password("{noop}123")
                .roles("CARS");
    }
}
