package com.example.JavaRiga13Ex.thymeleaf;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequiredArgsConstructor
@SessionAttributes("errors")
@RequestMapping("/pet")
public class PetController {

    private final PetRepo petRepo;

    @GetMapping
    public String showHomePage(final ModelMap modelMap) {
        modelMap.addAttribute("userName", SecurityContextHolder.getContext().getAuthentication().getName());
        modelMap.addAttribute("newPet", new Pet());
        return "welcome";
    }

    @GetMapping("/all")
    public String showAllPets(final ModelMap modelMap) {
        modelMap.addAttribute("petList", petRepo.findAll());
        return "pet-list";
    }

    @PostMapping("/save")
    public String savePet(final ModelMap modelMap, @Valid Pet pet, Errors errors) {
        if (errors.hasErrors()) {
            modelMap.addAttribute(
                    "errors",
                    errors.getAllErrors().stream()
                            .map(error -> error.getDefaultMessage())
                            .collect(Collectors.toList()));
        } else {
            petRepo.save(pet);
        }
        return "redirect:/pet";
    }
}
