package com.example.JavaRiga13Ex.thymeleaf;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank(message = "Pet name can't be empty")
    private String name;
    private int age;
    @NotBlank(message = "Pet type name can't be empty(CAT,MOUSE,HORSE)")
    private String type;
}
